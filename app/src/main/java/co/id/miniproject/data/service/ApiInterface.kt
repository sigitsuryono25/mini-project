package co.id.miniproject.data.service

import co.id.miniproject.data.model.DetailMovieModel
import co.id.miniproject.data.model.MovieModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {

    @GET("3/discover/movie")
    fun getMovies(
        @Query("api_key") api_key: String,
        @Query("language") language: String,
        @Query("sort_by") sort_by: String,
        @Query("page") page: Int
    ): Single<MovieModel.ResponseMovie>


    @GET("3/movie/{movie_id}")
    fun getDetailMovies(
        @Path("movie_id") movie_id: String,
        @Query("api_key") api_key: String,
        @Query("language") language: String
    ): Single<DetailMovieModel>

}