package co.id.miniproject.data.repository

import co.id.miniproject.data.mapper.DetailMovieMapper
import co.id.miniproject.data.service.ApiInterface
import co.id.miniproject.domain.DetailMovieDomain
import io.reactivex.Single

class DetailRepositoryImpl(
    private val service: ApiInterface,
    private val detailMovieMapper: DetailMovieMapper
) : DetailMovieRepository {
    override fun getDetailMovie(
        id: String,
        api: String,
        lang: String
    ): Single<DetailMovieDomain> {
        return service.getDetailMovies(id, api, lang).map {
            detailMovieMapper.mapToDomain(it)
        }
    }
}