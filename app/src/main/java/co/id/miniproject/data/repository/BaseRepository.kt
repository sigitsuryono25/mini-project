package co.id.miniproject.data.repository

import co.id.miniproject.domain.DetailMovieDomain
import co.id.miniproject.domain.MovieDomain
import io.reactivex.Single

interface MovieRepository {
    fun getMovies(
        apiKey: String,
        language: String,
        shortBy: String,
        page: Int
    ): Single<List<MovieDomain>>
}

interface DetailMovieRepository {
    fun getDetailMovie(
        id: String,
        api: String,
        lang: String
    ): Single<DetailMovieDomain>
}