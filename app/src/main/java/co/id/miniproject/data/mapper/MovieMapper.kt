package co.id.miniproject.data.mapper

import co.id.miniproject.data.model.MovieModel
import co.id.miniproject.domain.MovieDomain

class MovieMapper : BaseMapper<MovieModel, MovieDomain> {
    override fun mapToDomain(model: MovieModel): MovieDomain {
        return MovieDomain(
            model.overview,
            model.originalLanguage,
            model.originalTitle,
            model.video,
            model.title,
            model.genreIds,
            model.posterPath,
            model.backdropPath,
            model.releaseDate,
            model.popularity,
            model.voteAverage,
            model.id,
            model.adult,
            model.voteCount
        )
    }

    override fun mapToModel(domain: MovieDomain): MovieModel {
        return MovieModel(
            domain.overview,
            domain.originalLanguage,
            domain.originalTitle,
            domain.video,
            domain.title,
            domain.genreIds,
            domain.posterPath,
            domain.backdropPath,
            domain.releaseDate,
            domain.popularity,
            domain.voteAverage,
            domain.id,
            domain.adult,
            domain.voteCount
        )
    }
}