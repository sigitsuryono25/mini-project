package co.id.miniproject.ui.base

object Constant {
    const val BASE_URL = "https://api.themoviedb.org/"
    const val LINK_IMAGE = "https://image.tmdb.org/t/p/original/"
    const val LANG = "en-US"
    const val SORT_BY = "popularity.desc"
}