package co.id.miniproject.ui.movie

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import co.id.miniproject.R
import co.id.miniproject.domain.MovieDomain
import co.id.miniproject.helper.PaginationScrollListener
import co.id.miniproject.helper.toast
import co.id.miniproject.ui.base.LoadMoreItemView
import co.id.miniproject.ui.detail.DetailMovieActivity
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val movieVM: MovieVM by inject()
    private val adapterMovie = GroupAdapter<ViewHolder>()
    private var page = 1
    private var isLoadMore = false
    private var isLastPage = false
    private var loadMoreItemView = LoadMoreItemView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progressBarHolderLoadingCL.visibility = View.VISIBLE

        movieVM.movieState.observe(this, startObserver)
        movieVM.getMovies(page)
        setupRV()
    }

    private fun setupRV() {
        val linearLayout = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_movie.apply {
            layoutManager = linearLayout
            adapter = adapterMovie
        }

        rv_movie.addOnScrollListener(object : PaginationScrollListener(linearLayout) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoadMore
            }

            override fun loadMoreItems() {
                isLoadMore = true
                page++

                movieVM.getMovies(page)
            }
        })

    }

    private val startObserver = Observer<MovieState> { movieState ->
        when (movieState) {
            is LoadingState -> {
                if (isLoadMore) {
                    adapterMovie.add(loadMoreItemView)
                }
            }

            is MovieDataLoadedState -> {
                progressBarHolderLoadingCL.visibility = View.GONE
                if (isLoadMore) {
                    adapterMovie.remove(loadMoreItemView)
                    isLoadMore = false
                }

                if (page == 1) {
                    adapterMovie.clear()
                }

                movieState.movieDomain.map {
                    adapterMovie.add(MovieItemView(it, object : MovieItemView.OnItemClick {
                        override fun onClick(item: MovieDomain) {
                            val intent = Intent(this@MainActivity, DetailMovieActivity::class.java)
                            intent.putExtra(DetailMovieActivity.EXTRA_MOVIE, item.id.toString())
                            startActivity(intent)
                        }
                    }))
                }
            }

            is DataNotFoundState -> {
                adapterMovie.clear()
            }

            is LastPageState -> {
                if (isLoadMore) {
                    adapterMovie.remove(loadMoreItemView)
                    isLoadMore = false
                    if (!isLastPage) {
                        toast(this, "Last Page")
                        isLastPage = true
                    }
                }
            }

            is ErrorState -> {
                progressBarHolderLoadingCL.visibility = View.GONE
            }
        }
    }
}
