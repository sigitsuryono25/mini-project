package co.id.miniproject.ui.movie

import co.id.miniproject.R
import co.id.miniproject.domain.MovieDomain
import co.id.miniproject.helper.loadImageUrl
import co.id.miniproject.ui.base.Constant
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_movie.view.*

class MovieItemView(
    private val movieDomain: MovieDomain,
    private val onItemClick: OnItemClick
) : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        val image = viewHolder.itemView.imgMovie
        val name = viewHolder.itemView.tvMovieName
        val rating = viewHolder.itemView.tv_rating

        name.text = movieDomain.title
        rating.text = movieDomain.popularity.toString()
        movieDomain.posterPath?.let { image.loadImageUrl(Constant.LINK_IMAGE + it) }

        viewHolder.itemView.card_button.setOnClickListener {
            onItemClick.onClick(movieDomain)
        }
    }

    override fun getLayout(): Int {
        return R.layout.item_movie
    }

    interface OnItemClick {
        fun onClick(item: MovieDomain)
    }

}