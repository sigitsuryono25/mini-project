package co.id.miniproject.helper

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}