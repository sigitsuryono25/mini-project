package co.id.miniproject.di

import co.id.miniproject.data.mapper.DetailMovieMapper
import co.id.miniproject.data.mapper.MovieMapper
import co.id.miniproject.data.repository.DetailMovieRepository
import co.id.miniproject.data.repository.DetailRepositoryImpl
import co.id.miniproject.data.repository.MovieRepository
import co.id.miniproject.data.repository.MovieRepositoryImpl
import co.id.miniproject.data.service.ApiInterface
import co.id.miniproject.data.service.BaseInterceptor
import co.id.miniproject.ui.base.Constant
import co.id.miniproject.ui.detail.DetailMovieVM
import co.id.miniproject.ui.movie.MovieVM
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val appModule = module {
    single { BaseInterceptor() }
    single { createOkHttpClient(get()) }
    single { createWebService<ApiInterface>(get(), Constant.BASE_URL) }
}

val dataModule = module {

    //Repository
    single { MovieRepositoryImpl(get(), get()) as MovieRepository }
    single { DetailRepositoryImpl(get(), get()) as DetailMovieRepository }

    //Mapper
    single { MovieMapper() }
    single { DetailMovieMapper() }

    //View Model
    viewModel { MovieVM(get()) }
    viewModel { DetailMovieVM(get()) }
}

fun createOkHttpClient(interceptor: BaseInterceptor): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    val timeout = 60L
    return OkHttpClient.Builder()
        .connectTimeout(timeout, TimeUnit.SECONDS)
        .readTimeout(timeout, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor)
        .addInterceptor(interceptor)
        .build()
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T {
    val gson = GsonBuilder()
        .setDateFormat("yyyy-MM-dd HH:mm:ss")
        .create()
    val retrofit = Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
    return retrofit.create(T::class.java)
}

val myAppModule = listOf(appModule, dataModule)